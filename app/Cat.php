<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    //
    protected $fillable = ['name', 'id'];

    public function post()
    {
        return $this->hasMany(Post::class);
    }
    
    public function userpost()
    {
        return $this->hasMany(Userpost::class);
    }
}
